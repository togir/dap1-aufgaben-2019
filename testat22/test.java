class Test {
	
	public static void main(String[] args) {
		int[] arr = {1, 3, 2, 3, 4, 3, 6, 7};

		//1 System.out.println(quersumme(6707));
		//2 System.out.println(harm(5));
		//3 System.out.println(power(6, 4));
		//4 System.out.println(powerPlus(6, 4));
		//5 System.out.println(maximum(arr, 6));
		//6 System.out.println(isSorted(arr, 7));
		//7 System.out.println(countPositives(arr, 7, 3));
		//8 System.out.println(contentCheck(carr1, carr2, 3));
		//9 System.out.println(palindromCheck(carr1, 1));
		System.out.println(getIndex(arr, 7, 6));

	}

	public static int quersumme(int n) {
		if (n == 0) {
			return 0;
		}

		return quersumme(n/10) + n%10; 
	}

	public static double harm(int n) {
		if( n == 0) {
			return 0;
		}

		return harm(n - 1) + (double) 1/n;
	}

	public static int power(int a, int n) {
		if (n == 0) {
			return 1;
		}

		return power(a, n -1) * a;
	}

	public static int powerPlus(int a, int n) {
		if (n == 0) {
			return 1;
		}

		if( n%2 == 0) {
			return power(a, n/2) * power(a, n/2);
		}

		return a * power(a, (n-1)/2) * power(a, (n-1)/2);

	}

	public static String binaryCode(int i) {
		if(i / 2 == 0) {
			return String.valueOf(i%2);
		}

		return binaryCode(i/2) + String.valueOf(i%2) ;
	}

	public static int maximum(int[] arr, int i){
		if(i == 0) {
			return arr[i];
		}

		if( arr[i] > maximum(arr, i - 1)) {
			return arr[i];
		}	

		return maximum(arr, i -1);
	}

	public static boolean isSorted(int[] arr, int i) {

		if(i == 0) {
			return true;
		}

		if(arr[i -1] > arr[i]) {
			return false;
		}

		return isSorted(arr, i - 1);
	}

	public static boolean contains(int[] arr, int i, int x) {
		if(i < 0) {
			return false;
		}

		if(arr[i] == x ){
			return true; 
		}
		
		return contains(arr, i - 1, x);
	}

	public static boolean contentCheck(char[] arr1, char[] arr2, int i) {

		if(i < 0) {
			return true;
		}

		if( arr1[i] != arr2[i]) {
			return false;
		}

		return contentCheck(arr1, arr2, i - 1);
	}

	public static int getIndex(int[] arr, int i, int x) {

		// -1, da out of bounds
		if (i < 0 || i >= arr.length) {
			return -1;
		}

		// Wenn x gefunden wird und es im kleineren Bereich des Array kein weiters x gibt
		if(arr[i] == x && getIndex(arr, i - 1, x) == -1 ) {
			return i;
		}

		// rekursiver Aufruf mit nächst kleinerem Element
		return getIndex(arr, i - 1, x);

	}

	public static int count(int[] arr, int i) {
		if (i < 0 || i >= arr.length) {
			return 0;
		}

		if(arr[i] == 0) {
			return count(arr, i -1) +1;
		}

		return count(arr, i -1);
	}
}
