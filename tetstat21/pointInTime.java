package tetstat21;

import java.time.Year;

class PointInTime {
    private int jahr, tag, stunde;

    public PointInTime (int jahrn, int tagn, int stunden) {

        if (jahrn < 1000 || jahrn > 9999) {
            throw new IllegalArgumentException("Parameter 1(jahr) muss 4stellig sein");
        }

        if (tagn < 0 || tagn > 365) {
            throw new IllegalArgumentException("Paramenter 2(tag) tag muss im Intervall 1 bis 365 liegen");
        }

        if(stunden < 0 || stunden > 24) {
            throw new IllegalArgumentException("Parameter 3(stunde) muss im Intervall 0 bis 24 liegen ");
        }

        jahr = jahrn;
        tag = tagn;
        stunde = stunden;
    }

    /**
     * @return the jahr
     */
    public int getJahr() {
        return jahr;
    }

    /**
     * @return the tag
     */
    public int getTag() {
        return tag;
    }

    /**
     * @return the stunde
     */
    public int getStunde() {
        return stunde;
    }

    public String toString() {
        return  jahr + "/" + tag + "/" + stunde;
    }

    public PointInTime clone() {
        return new PointInTime(jahr, tag, stunde);
    }

    public void changem(int stunden) {

        if (stunden < 0) {
            throw new IllegalArgumentException("Parameter stunden darf nicht negativ sein");
        }

        int altStunde = stunde;
        int altTag = tag;
        int altJahr = jahr;

        stunde = stunden + stunde;

        if(stunde > 23) {
            int daysToAdd = stunde / 23;
            tag = tag + daysToAdd;
            stunde = stunde % 23;
        }

        if (tag > 364) {
            int yearsToAdd = tag / 364;
            jahr = yearsToAdd + jahr;
            tag = tag % 364;
        }

        if(jahr > 9999) {
            stunde = altStunde;
            tag = altTag;
            jahr = altJahr;
            throw new IllegalArgumentException("Parameter stunden ist zu groß!");
        }
    }


    public void change(int cstunde){
        int altstunde = stunde;
        int alttag = tag ;
        int altjahr = jahr ;
        
        if ( cstunde > -1){
            stunde = stunde + cstunde;
            
            if ( stunde > 24){
                tag = tag + stunde / 24;
                stunde = stunde % 24;
                    
                if ( tag > 364){
                    jahr = jahr + tag /  365 ;
                    tag = tag % 365;
                    
                    if(jahr > 9999) {
                        stunde = altstunde;
                        tag= alttag;
                        jahr = altjahr;
                        throw new IllegalArgumentException("Parameter Stunde ist zu gross");
                    }
                }

            }
        }
    }

}