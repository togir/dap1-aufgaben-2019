package tetstat21;

class Period {

    private int minutes;

    public Period(int m) {
        setMinutes(m);
    }

    public Period (int hours, int min) {

        setMinutes(hours * 60 + min);
    }

    private void setMinutes(int m) {

        if (m < 0) {
            minutes = 0;
        } else {
            minutes = m;
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public int getHours() {
        return minutes / 60;
    }

    public int getMinorMinutes() {
        return minutes % 60;
    }

    public String toString() {
        return getHours() + ":" + getMinorMinutes();
    }


    public Period clone() {
        return new Period(minutes);
    }

    public void change(int m) {
        if (m >= 0) {
            setMinutes(minutes + m);
        }
    }
}