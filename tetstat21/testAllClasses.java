package tetstat21;

class TestAllClasses {

    public static void main(String[] args) {
        Period period = new Period(30);
        Period period2 = period.clone();

        System.out.println(period);
        System.out.println(period2);

        PointInTime point = new PointInTime(2019, 360, 8);
        PointInTime point2 = point.clone();

        System.out.println(point);
        System.out.println(point2);


        Date date = new Date(point, period);
        Date date2 = new Date(point2, period2);

        date2.change(2);

        System.out.println(date);
        System.out.println(date2);

    }
}