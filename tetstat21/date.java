package tetstat21;

class Date {
    private PointInTime start = null;
    private Period period = null;

    public Date(PointInTime startn, Period periodn) {
        start = startn;
        period = periodn;
    }

    public Date(PointInTime startn) {
        start = startn;
    }

    public Date(Period periodn) {
        period = periodn;
    }

    public Date() {

    }

    /**
     * @return the start
     */
    public PointInTime getStart() {
        return start;
    }

    /**
     * @return the period
     */
    public Period getPeriod() {
        return period;
    }

    public Date clone() {
        return new Date(start, period);
    }

    public void set(PointInTime startn, Period periodn) {
        start = startn;
        period = periodn;
    }

    public void change (int hours) {

        if (start == null){
            throw new IllegalCallerException("Es ist kein Startwert vorhanden");
        }
        start.change(hours);
    }

    public String toString() {
        return "Datum: " + start.toString() +" Dauer: " + period.toString();
    }

}