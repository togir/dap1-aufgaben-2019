package tetstat21;

class Testat21 {
    public static void main(String[] args) {

        PointInTime point1 = new PointInTime(2019, 360, 8);
        Period period1 = new Period(30);

        Date date1 = new Date(point1, period1);
        date1.change(2);

        System.out.println(date1.toString());

    }
}