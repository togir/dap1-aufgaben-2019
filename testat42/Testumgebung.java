public class Testumgebung
{
    public static void main( String[] args )
    {
        IntIntPairs testPairs = new IntIntPairs( 20 );
        testPairs.put(7,3); testPairs.put(4,3); testPairs.put(1,-9); testPairs.put(6,6); testPairs.put(2,8); 
        testPairs.put(41,12); testPairs.put(9,-11); testPairs.put(8,12); testPairs.put(5,-60); testPairs.put(14,18); testPairs.put(0,12); testPairs.put(-9,-16); testPairs.put(0,0); testPairs.put(17,32); testPairs.put(1,1);

        testPairs.show();
        System.out.println("----------------------------------------------------");

        /*
        Aufgabe 1.1 
        System.out.println("Beispiel: " + testPairs.accumulate( (k,v) -> { if (k==0) { return v; } else { return 0; } } ) );

        System.out.print("Beispiel: " + sumUp( testPairs ) ); System.out.println();

        Aufgabe 1.2
        
        System.out.println(testPairs.accumulate((k, v) -> k % 2 == 0 ? 1 : 0 ));
        

        // Aufgabe 1.3
        testPairs.remove((k, v) -> k == 6);
        testPairs.show();
        

        // Aufgabe 1.4
        testPairs.manipulate((k, v) -> k+10, (k, v) -> v);
        testPairs.show();
        

        // Aufgabe 1.5
        IntIntPairs tmpPairs = testPairs.extract((k, v) -> v%3 == 0);
        tmpPairs.show();


        // Aufgabe 1.6
        System.out.println(testPairs.accumulate((k, v) -> v > 10 ? 1: 0));


        // Aufgabe 1.7
        testPairs.manipulate((k, v) -> v > 3 ? k+5: k, (k, v) -> v);
        testPairs.show();
        

        // Aufgabe 1.8
        testPairs.remove((k, v) -> v < 0);
        testPairs.show();
        

        // Aufgabe 1.9 */
        testPairs.manipulate((k,v) -> k/3, (k, v) -> v/3);
        testPairs.show();
        /*

        // Aufgabe 1.10
        IntIntPairs tmpPair = testPairs.extract((k, v) -> true);
        tmpPair.show();
        

        // Aufgabe 1.11
        testPairs.remove((k, v) -> k==v);
        testPairs.show();
        

        // Aufgabe 1.12
        System.out.println(testPairs.accumulate((k, v) -> v));


        // Aufgabe 1.13
        IntIntPairs tmpPairs = testPairs.extract((k, v) -> v > 5);
        tmpPairs.show();
        
        */
    }





    public static int sumUp(IntIntPairs pairs)
    {
        return pairs.accumulate( (k,v) -> { if (k==0) { return v; } else { return 0; } } );
    }
    
//     public static void addNToValue( IntIntPairs pairs, int n )
//     {
//         // hier ergaenzen
//     }
//     
//     public static boolean uniqueKey( IntIntPairs pairs, int n )
//     {
//         // hier ergaenzen        
//     }
//     
//     public static void doubleGreaterN( IntIntPairs pairs, int n )
//     {
//         // hier ergaenzen
//     }
//     
//     public static IntIntPairs concat(IntIntPairs first, IntIntPairs second)
//     {
//         // hier ergaenzen
//     }
}
