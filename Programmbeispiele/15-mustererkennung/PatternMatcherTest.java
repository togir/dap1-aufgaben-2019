public class PatternMatcherTest
{
    public static void main( String[] args )
    {
        test();
    }

    public static void test()
    {
        String testtext = "abdbababaaababaabaabccababaedl�lklnnabxbabxbaaajkksssaaba";
        System.out.println( PatternMatcher.containsAt( "saab", testtext ) );
        System.out.println( PatternMatcher.containsAt( "saub", testtext ) );
        System.out.println( PatternMatcher.contains( "saab", testtext ) );
        System.out.println( PatternMatcher.contains( "saub", testtext ) );
        System.out.println( PatternMatcher.count( "ab", testtext ) );
        System.out.println( PatternMatcher.count( "baa", testtext ) );
        System.out.println( PatternMatcher.count( "ub", testtext ) );
    }
}
