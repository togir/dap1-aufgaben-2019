import java.util.*;

public class PatternMatcher
{
    private static Set<String> generatePrefixes( String s )
    {
        Set<String> result = new HashSet<String>();
        for( int i = 0; i <= s.length(); i++ )
        {
            result.add( s.substring( 0, i ) );
        }
        return result;
    }

    private static List<String> generateSuffixes( String s )
    {
        List<String> result = new LinkedList<String>();
        for( int i = 0; i < s.length(); i++ )
        {
            result.add( s.substring( i, s.length() ) );
        }
        return result;
    }

    public static Set<Character> generateTokens( String s )
    {
        Set<Character> result = new HashSet<>();
        for ( int i = 0; i < s.length(); i++ )
        {
            result.add( s.charAt( i ) );
        }
        return result;
    }

    private static Map<Integer,Map<Character,Integer>> generateTransitionTable( String p )  // Rueckgabetyp geaendert
    {
        Map<Integer,Map<Character,Integer>> transitionTable = new HashMap<>();    // Typ der Referenz geaendert
        Set<Character> tokens = generateTokens( p );
        Set<String> prefixes = generatePrefixes( p );
        for ( String prefix : prefixes )
        {
            for ( char c : tokens )
            {
                String continuation = prefix + c;
                List<String> suffixes = generateSuffixes( continuation );
                for ( String suffix : suffixes )
                {
                    if ( prefixes.contains( suffix ) )    
                    {
                        if ( ! transitionTable.containsKey( prefix.length() ) )    // pruefen, ob key schon vorhanden, sonst Map-Objekt erzeugen
                        {
                            transitionTable.put( prefix.length(), new HashMap<Character,Integer>() );   // Map-Objekt erzeugen
                        }
                        transitionTable.get( prefix.length() ).put( c , suffix.length() );  // Uebergang eintragen
                        break;
                    }
                }
            }
        }
        return transitionTable;
    }

    public static int containsAt( String p, String text )
    {
        Map<Integer,Map<Character,Integer>> transitionTable = generateTransitionTable( p );   // Typ der Referenz geaendert
        int state = 0;
        final int FINALSTATE = p.length();
        for ( int position = 0; position < text.length(); position++ )
        {
            if ( transitionTable.containsKey( state ) && transitionTable.get( state).containsKey( text.charAt( position ) ) )  // pruefen, ob passender Eintrag vorhanden ist
            {
                state = transitionTable.get( state ).get( text.charAt( position ) );  // Folgezustand ermitteln
                if ( state == FINALSTATE )
                {
                    return position - p.length() + 1;
                }
            }
            else 
            {
                state = 0;
            }

        }
        return -1;
    }   

    public static boolean contains( String p, String text )
    {
        return containsAt( p, text ) != -1;
    }  

    public static int count( String p, String text )
    {
        Map<Integer,Map<Character,Integer>> transitionTable = generateTransitionTable( p );  
        int quantity = 0;
        int state = 0;
        final int FINALSTATE = p.length();
        for ( int position = 0; position < text.length(); position++ )
        {
            if ( transitionTable.containsKey( state ) && transitionTable.get( state).containsKey( text.charAt( position ) ) )  
            {
                state = transitionTable.get( state ).get( text.charAt( position ) );  
                if ( state == FINALSTATE )
                {
                    quantity++;
                }
            }
            else 
            {
                state = 0;
            }
        }
        return quantity;
    }   

}
