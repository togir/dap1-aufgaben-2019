import java.util.Iterator;

public class DoublyLinkedList<T>
implements Iterable<T>
{
    private Element<T> first, last;
    private int size;

    public DoublyLinkedList()
    {
        first = last = null;
        size = 0;
    }

    public int size()
    {
        return size;
    }

    public boolean isEmpty()
    {
        return size == 0;
    }

    public void add( T content ) 
    {
        Element<T> e = new Element<T>( content );
        if ( isEmpty() ) 
        {
            first = last = e;
        }
        else 
        {
            last.connectAsSucc( e );
            last = e;
        }
        size++;
    }

    public void addFirst( T content ) 
    {
        Element<T> e = new Element<T>( content );
        if ( isEmpty() ) 
        {
            first = last = e;
        }
        else 
        {
            first.connectAsPred( e );
            first = e;
        }
        size++;
    }

    public T getFirst()
    throws NoSuchElementException
    {
        if ( !isEmpty() )
        {
            return first.getContent();
        }
        else
        {
            throw new NoSuchElementException();
        }
    }

    public T get( int index )
    {
        if ( index >= 0 && index < size )
        {
            Element<T> current = first;
            for ( int i = 0; i < index; i++ )
            {
                current = current.getSucc();
            }
            return current.getContent();
        }
        else
        {
            throw new IllegalIndexException();
        }
    }

    public T removeFirst()
    throws NoSuchElementException
    {
        if ( !isEmpty() ) 
        {
            T result = first.getContent();
            if ( first.hasSucc() )
            {
                first = first.getSucc();
                first.disconnectPred();
            }
            else
            {
                first = last = null;
            }
            size--;
            return result;
        }
        else
        {
            throw new NoSuchElementException();
        }
    }

    public void showAll()
    {
        Element<T> current = first;
        while ( current != null )
        {
            System.out.print( current.getContent().toString() );
            if ( current != last )
            {
                System.out.print(", ");
            }
            current = current.getSucc();
        }
        System.out.println();
    }

    // Iterator

    public Iterator<T> iterator()
    {
        return new ForwardIterator<T>( first );
    }

    private static abstract class ListIterator<I> implements Iterator<I>
    {
        protected Element<I> current;

        protected ListIterator( Element<I> elem )
        {
            current = elem;
        }

        public boolean hasNext()
        {
            return current != null;
        }

        public I next()
        {
            if ( hasNext() )
            {
                I content = current.getContent();
                current = step();
                return content;
            }
            else
            {
                throw new IllegalStateException();
            }
        }

        protected abstract Element<I> step();
    
    }

    private static class ForwardIterator<F> extends ListIterator<F>
    {
        public ForwardIterator( Element<F> elem )
        {
            super( elem );
        }

        protected Element<F> step()
        {
            return current.getSucc();
        }

    }
    
    // Element

    private static class Element<E>
    {
        private E content;
        private Element<E> pred, succ;

        public Element( E c )
        {
            content = c;
            pred = succ = null;
        }

        public E getContent()
        {
            return content;
        }

        public void setContent( E c )
        {
            content = c;
        }

        public boolean hasSucc()
        {
            return succ != null;
        }

        public Element<E> getSucc()
        {
            return succ;
        }

        public void disconnectSucc()
        {
            if ( hasSucc() ) 
            {
                succ.pred = null;
                succ = null;
            }
        }

        public void connectAsSucc( Element<E> e)
        {
            disconnectSucc();
            if ( e != null ) 
            {
                e.disconnectPred();
                e.pred = this;
            }
            succ = e;
        }

        public boolean hasPred()
        {
            return pred != null;
        }

        public Element<E> getPred()
        {
            return pred;
        }

        public void disconnectPred()
        {
            if ( hasPred() )
            {
                pred.succ = null;
                pred = null;

            }
        }

        public void connectAsPred( Element<E> e )
        {
            disconnectPred();
            if ( e != null )
            {
                e.disconnectSucc();
                e.succ = this;
            }
            pred = e;
        }
    }

}
