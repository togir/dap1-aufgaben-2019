import java.util.Iterator;

public class IterableTest
{
    public static void main( String[] args )
    {
        test();
    }

    public static <F> void printData( Iterable<F> toIter )
    {
        Iterator<F> it = toIter.iterator();
        while ( it.hasNext() )
        {
            System.out.println( it.next() );
        }
    }
    
    public static void test()
    {
        DoublyLinkedList<Integer> intsList = new DoublyLinkedList<>();
        intsList.add( 18 );
        intsList.add( 14 );
        intsList.add( 22 );
        intsList.add( 7 );
        intsList.add( 17 );
        intsList.add( 20 );
        printData( intsList );
    }
}
