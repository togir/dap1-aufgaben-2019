import static java.lang.System.out;

public class DllTest
{
    public static void main( String[] args )
    {
        testDoublyLinkedList();
    }
        
    public static void testDoublyLinkedList()
    {
        DoublyLinkedList students = new DoublyLinkedList();
        students.add( new Student( "A", "Inf", 123433 ) );
        students.add( new Student( "B", "Inf", 123456 ) );
        students.add( new Student( "C", "Inf", 123457 ) );
        students.add( new Student( "D", "Inf", 123458 ) );
        students.add( new Student( "E", "Inf", 123459 ) );
        students.add( new Student( "F", "Inf", 123460 ) );

        /* Aufgabe 1 
        students.clear();
        students.showAll();
        */

        /* Aufgabe 2 
        out.println(students.getLast().getContent());
        */

        /* Aufgabe 3 
        out.println(students.contains(new Student("C", "Inf", 123457)));
        */
        


        DoublyLinkedList numbers = new DoublyLinkedList();
        numbers.add( new Fraction( 2, 3 ) );
        numbers.add( new Fraction( 1, 7 ) );
        numbers.add( new Fraction( 3, 12 ) );
        numbers.add( new Fraction( 8 ) );

        /* Aufgabe 1 
        numbers.clear();
        numbers.showAll();
        */

        /* Aufgabe 2 
        out.println(numbers.getLast().getContent());
        */

        /* Aufgabe 3 
        out.println(numbers.contains(new Fraction(1, 7)));
        */


        DoublyLinkedList integers = new DoublyLinkedList();
        integers.add( 0 );
        integers.add( 1 );
        integers.add( 2 );
        integers.add( 3 );
        integers.add( 3 );
        integers.add( 4 );
        integers.add( 4 );
        integers.add( 4 );
        integers.add( 5 );


        


        /* Aufgabe 1 
        integers.clear();
        integers.showAll();
        */

        /* Aufgabe 2 
        out.println(integers.getLast().getContent());
        */

        /* Aufgabe 3 
        out.println(integers.contains(null));
        */

        /* Aufgabe 5 
        DoublyLinkedList empty = new DoublyLinkedList();
        out.println(empty.allEqual());
        */
        

        /* Aufgabe 6 
        out.println(integers.containsDoubleJ());

        DoublyLinkedList equalList = new DoublyLinkedList();
        equalList.add(1);
        equalList.add(1);
        equalList.add(1);

        out.println(equalList.containsDoubleJ());
        */
        

        /* Aufgabe 7
        integers.showAll();
        integers.insert(4, 5);
        integers.showAll();
        */

        /* Aufgabe 8 
        FRAGE: Inhalte der Elements oder das Elemenst?
        Object[] arr = new Object[3];

        for (int i = 0; i < arr.length; i++) {
            out.println(arr[i]);
        }
    
        integers.toArray(arr);

        for (int i = 0; i < arr.length; i++) {
            out.println(arr[i]);
        }

        */

        /* Aufgabe 8 
        integers.showAll();
        DoublyLinkedList newInt = integers.flip();
        newInt.showAll();
        */

        /* Aufgabe 9 

        integers.showAll();
        integers.remove(2);
        integers.showAll();

        */

        /* Aufgabe 10

        integers.showAll();
        integers.remove((Object) 9);
        integers.showAll();
         */

        
        /* Aufgabe 12
        DoublyLinkedList equalList = new DoublyLinkedList();
        equalList.add(1);
        equalList.add(4);
        equalList.add(1);

        integers.showAll();
        integers.concat(equalList);
        integers.showAll();
        equalList.showAll();
        */

        /* Aufgabe 13 
        DoublyLinkedList equalList = new DoublyLinkedList(integers);

        integers.showAll();
        equalList.showAll();
        */

        /* Aufagbe 14
        integers.showAll();
        DoublyLinkedList newList = integers.subListJ(2, 4);
        newList.showAll();
        */


        /* Aufgabe 15 
        DoublyLinkedList equalList = new DoublyLinkedList();
        equalList.add(1);
        equalList.add(4);

        integers.showAll();
        equalList.showAll();

        integers.removeAll(equalList);
        integers.showAll();


        DoublyLinkedList list = new DoublyLinkedList();
        list.add( new Student( "A", "Inf", 123433 ) );
        list.add( new Fraction( 3, 7 ) );
        */
        

        /* Aufgabe 16 
        integers.showAll();
        integers.pack();
        integers.showAll();
        */

        integers.showAll();
        integers.jorgo2(new Integer(4), new Integer(6));
        integers.showAll();

        /* Aufgabe 1
        list.clear();
        list.showAll();
        */

        /* Aufgabe 2 
        out.println(list.getLast().getContent());
        */

        /* Aufgabe 3
        out.println(list.contains(new Fraction(2,2)));
        */

        /* Aufgabe 13 
        DoublyLinkedList newList = new DoublyLinkedList(list);
        list.showAll();
        newList.showAll();
        */



       // DoublyLinkedList emptyList = new DoublyLinkedList();

        /* Aufgabe 1
        list.clear();
        list.showAll();
        */

        /* Aufgabe 2 
        out.println(emptyList.getLast().getContent());
        */

        /* Aufgabe 3 
        out.println(emptyList.contains(new Object()));
        */
    }
}
