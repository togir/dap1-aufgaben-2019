public class DoublyLinkedList
{
    private Element first, last;
    private int size;

    public DoublyLinkedList()
    {
        first = last = null;
        size = 0;
    }

    /* Aufgabe 13 */
    public DoublyLinkedList(DoublyLinkedList dll) {

        Element current = dll.first;

        while(current != null) {
            this.add(current.getContent());
            current = current.getSucc();
        }
    }

    public int size()
    {
        return size;
    }
    
    public boolean isEmpty()
    {
        return size == 0;
    }

    public void add( Object content ) 
    {
        Element e = new Element( content );
        if ( isEmpty() ) 
        {
            first = last = e;
        }
        else 
        {
            last.connectAsSucc( e );
            last = e;
        }
        size++;
    }

    public void addFirst( Object content ) 
    {
        Element e = new Element( content );
        if ( isEmpty() ) 
        {
            first = last = e;
        }
        else 
        {
            first.connectAsPred( e );
            first = e;
        }
        size++;
    }

    public Object getFirst() 
    {
        if ( !isEmpty() )
        {
            return first.getContent();
        }
        else
        {
            throw new RuntimeException();
        }
    }

    public Object get( int index ) 
    {
        if ( index >= 0 && index < size )
        {
            Element current = first;
            for ( int i = 0; i < index; i++ )
            {
                current = current.getSucc();
            }
            return current.getContent();
        }
        else
        {
            throw new RuntimeException();
        }
    }

    public Object removeFirst()
    {
        if ( !isEmpty() ) 
        {
            Object result = first.getContent();
            if ( first.hasSucc() )
            {
                first = first.getSucc();
                first.disconnectPred();
            }
            else
            {
                first = last = null;
            }
            size--;
            return result;
        }
        else
        {
            throw new RuntimeException();
        }
    }

    public void showAll()
    {
        Element current = first;
        while ( current != null )
        {
            System.out.print( current.getContent().toString() );
            if ( current != last )
            {
                System.out.print(", ");
            }
            current = current.getSucc();
        }
        System.out.println();
    }

    // Aufgabe 1
    public void clear() {

        first = last = null;
        size = 0;
    }

    // Aufgabe 2
    public Element getLast() {
        if(isEmpty()) {
            throw new IllegalStateException();
        }

        return this.last;
    }

    // Aufgabe 3
    public boolean contains(Object o) {
        
        Element current = this.first;
        while (current != null) {
            if (o != null && o.equals(current.getContent())) {
                return true;
            }
            if(o == null && current.getContent() == null ){
                return true;
            }

            current = current.getSucc();
        }

        return false;
    }

    // Aufgabe 4
    public int count(Object o) {

        int count = 0;
        Element current = this.first;

        while (current != null) {
            if (current.getContent().equals(o)) {
                count++;
            }
            current = current.getSucc();
        }

        return count;
    }


    // Aufgabe 5
    public boolean allEqual() {
        
        if(isEmpty()) {
            return true;
        }

        Element first = this.first;
        Element current = first.getSucc();

        while(current != null) {

            if (!current.getContent().equals(first.getContent())) {
                return false;
            }
            current = current.getSucc();
        }

        return true;
    }

    // Aufgabe 6
    public boolean containsDoubleWithCount() {

        
        Element current = this.first;
        
        while(current != null) {

            if(this.count(current.getContent()) >= 2) {
                return true;
            }

            current = current.getSucc();  
        }

        return false;
    }

    public boolean containsDouble() {

        
        Element current = this.first;
        
        while(current != null) {

            DoublyLinkedList newList = this;
            newList.first = current;

            Element newCurrent = newList.first.getSucc();

            while ( newCurrent != null) {
                if(newCurrent.getContent().equals(current.getContent())) {
                    return true;
                }
                newCurrent = newCurrent.getSucc();
            }

            current = current.getSucc();
        }

        return false;
    }

    public boolean containsDoubleJ (){
        
        Element current = first ;
        
        while ( current != null ){
            DoublyLinkedList newl = this;
            Element newlcurrent = newl.first;
            
            while ( newlcurrent != null){
            
                if ( current.getContent().equals(newlcurrent.getContent())){
                    return true;
                }
                newlcurrent = newlcurrent.getSucc();
            }
            current = current.getSucc();
        }
        return false;
    }
    

    // Aufgabe 7
    public void insert(int n, Object o) {
        if(n > this.size || n < 0) {
            throw new IndexOutOfBoundsException();
        }

        if (n == this.size) {
            this.add(o);
            return;
        }

        Element current = this.first;
        for(int i = 0; i < n; i++) {
            current = current.getSucc();
        }

        
        Element tmp = current.getSucc();
        current.connectAsSucc(new Element(o));
        current.getSucc().connectAsSucc(tmp);
        this.size++;
    }

    // Aufgabe 8
    public void toArray(Object[] arr) {

        int length = arr.length;
        Element current = this.first;

        for(int i=0; i < length; i++) {
            arr[i] = current.getContent();

            if(current.hasSucc()) {
                current = current.getSucc();
            } else {
                current = new Element(null);
            }
        }
    }

    // Aufgabe 9
    public DoublyLinkedList flip() {

        Element current = this.last;
        DoublyLinkedList newList = new DoublyLinkedList();

        while( current != null) {

            newList.add(current.getContent());

            current = current.getPred();
        }
        return newList;
    }

    // Aufgabe 10
    public void remove (int n) {
        
        if( this.size <= n || n < 0) {
            throw new IndexOutOfBoundsException();
        }

        if(n== 0 && n == size-1) {
            this.first = this.last = null;
            this.size = 0;
            return;
        }

        if( n == 0) {
            this.removeFirst();
            return;
        }

        if (n == size-1) {
            this.last = this.last.getPred();
            this.last.connectAsSucc(null);
            this.size--;
            return;
        }

        Element current = this.first;

        for (int i = 0; i < n; i++) {
            current = current.getSucc();
        }

        Element elBefore = current.getPred();
        Element elNext = current.getSucc();

        elBefore.connectAsSucc(elNext);
        elNext.connectAsPred(elBefore);

        size--;

    }

    // Aufgabe 11
    public void remove(Object o) {

        if(isEmpty()) {
            return;
        }
        
        Element current = this.first;
        int newSize = this.size;

        for (int i = 0; i < this.size; i++) {

            if((current.getContent() != null && current.getContent().equals(o)) || (current.getContent() == null && o == null)) {

                if(i == 0) {
                    this.first = current.getSucc();
                    current.disconnectSucc();
                    current = this.first;

                } 
                if (i == this.size){
                    this.last = current.getPred();
                    current.disconnectPred();
                } else {

                    Element elBefore = current.getPred();
                    Element elNext = current.getSucc();
    
                    elBefore.connectAsSucc(elNext);
                    // Set current back to previos element because connetcAsScucc has removed the succ and pred values of the "current" Element
                    current = elBefore;
                }
                newSize--;
            }
            
            if( current != null) {
                current = current.getSucc();
            }
        }

        this.size = newSize;
        
    }


    // Aufgabe 12
    public void concat( DoublyLinkedList dll) {
        
        while (dll.first != null) {
            this.add(dll.first.getContent());
            dll.removeFirst();
        }
    }

    // Aufgabe 13
    public DoublyLinkedList subList(int from, int to) {
        
        if(from < 0 || to >= this.size || from > this.size || from > to) {
            throw new IndexOutOfBoundsException();
        }

        DoublyLinkedList newList = new DoublyLinkedList();

        Element current = this.first;
        for (int i = 0; i < this.size; i++) {


            if(i >= from && i <= to ) {
                newList.add(current.getContent());
            }
            current = current.getSucc();
            
        }

        return newList;
    }

    
    public DoublyLinkedList subListJ( int from , int to ){
        if ( from >= 0 && from < to && to < size ){ 
            DoublyLinkedList newl = new DoublyLinkedList ();
            Element current = first;
            for ( int i = 0; i < from ; i++){
                current = current.getSucc();
            }
            for ( int k = 0 ; k < to -1 ; k++){
                Element p = new Element ( current.getContent());
                newl.add( p.getContent());
                current = current.getSucc();
            }


            return newl;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    // Aufgabe 15
    public void removeAll(DoublyLinkedList dll) {

        Element dllCurrent = dll.first;

        while(dllCurrent != null) {
            
            Element current = this.first;
            
            while(current != null) {

                if(current.getContent().equals(dllCurrent.getContent())) {
                    
                    if(this.first == current) {
                        this.removeFirst();

                    } else if(this.last == current) {
                        this.last = current.getPred();
                        current.disconnectPred();
                        this.size--;

                    } else {
                        Element elBefore = current.getPred();
                        Element elNext = current.getSucc();
        
                        elBefore.connectAsSucc(elNext);
                        // Set current back to previos element because connetcAsScucc has removed the succ and pred values of the "current" Element
                        current = elBefore;
                        this.size--;
                        }
                }

                current = current.getSucc();
            }

            dllCurrent = dllCurrent.getSucc();
        }
    }

    public void removeAllJ ( DoublyLinkedList dll ){

        Element dllcurrent = dll.first;

        while ( dllcurrent != null){
            Element current = first;

            while ( current != null){                
                if ( dllcurrent.getContent().equals( current.getContent())){
                    if ( current == first ){
                        removeFirst();
                    }
                    else if( current == last){
                        Element vorlast = last.getPred();
                        vorlast.disconnectSucc();
                        vorlast = last;
                        size--;
                    }
                    else{
                        Element vorcurrent = current.getPred() ;
                        Element nachcurrent = current.getSucc();
                        vorcurrent.connectAsSucc( nachcurrent);
                        current = vorcurrent;
                        size--;
                    }
                }
                current = current.getSucc();
            }
            dllcurrent = dllcurrent.getSucc();
        }

    }

    // Aufgabe 16
    public void pack() {
        Element current = first;

        while(current != null && current.getSucc() !=null) {
            if(current.getContent().equals(current.getSucc().getContent())) {

                Element toDelete = current.getSucc();
                
                if(toDelete == this.last) {
                    this.last = current;
                    current.disconnectSucc();
                } else {
                    current.connectAsSucc(toDelete.getSucc());
                    current.getSucc().connectAsPred(current);
                    this.size--;

                    if(current.getPred() != null) {
                        current = current.getPred();
                    } else {
                        current = first;
                    }
                }

            }

            current = current.getSucc();
        }
    }

    public void jorgo1(Object o) {

        if(this.size %2 != 0) {
            return;
        }
        
        if(isEmpty() || size < 2) {
            this.add(o);
        }

        Element current = first;

        for (int i = 1; i < size / 2; i++) {
            current = current.getSucc();
        }

        Element next = current.getSucc();
        current.connectAsSucc(new Element(o));
        current.getSucc().connectAsSucc(next);

    }

    public void jorgo2 (Object o, Object p) {

        if(isEmpty()) {
            return;
        }

        Element current = this.first;

        while(current != null) {

            if(current.getContent().equals(o)) {
                this.add(p);
                return;
            }

            current = current.getSucc();
        }
    }

    public void Jorgo2J( Object o , Object p){

        if ( isEmpty() ){
            return;
        }
        Element current = first;
        Element einzu = new Element (p);
        while ( current != null){
            
            if ( current.getContent().equals( o )){
                add( einzu.getContent());
            }
            current = current.getSucc();
        }

    }
}
