public class AddNToPositivesStrategy implements DoublyLinkedList.SubstitutionStrategy<Integer> {

    private int n;

    public AddNToPositivesStrategy(int n) {
        this.n = n;
    }

    public Integer substitute(Integer ref) {
        if((Integer) ref > 0) {
            return (Integer) ref + n;
        }

        return ref;
    }
}