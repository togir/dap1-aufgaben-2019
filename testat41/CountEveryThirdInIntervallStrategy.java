public class CountEveryThirdInIntervallStrategy implements DoublyLinkedList.InspectionStrategy<Integer> {

    private int index, from, to, count, sum;

    public CountEveryThirdInIntervallStrategy(int from, int to) {
        this.index = this.count = this.sum = 0;
        this.from = from;
        this.to = to;
    }

    public void inspect(Integer el){
        if(this.index >= this.from && this.index <= this.to) {

            this.count = this.count +1;

            if(this.count % 3 == 0) {
                this.count = 0;
                this.sum = this.sum + el;
            }
        }

        this.index = this.index + 1;
    }

    public int getSum() {
        return this.sum;
    }
}