public class SubtotalStrategy implements DoublyLinkedList.InsertionStrategy<Integer> {

    private int subSum;


    public SubtotalStrategy() {
        this.subSum = 0;
    }

    @Override
    public boolean select(Integer ref) {
        subSum = subSum + ref;
        return true;
    }

    @Override
    public Integer insert(Integer ref) {
        return subSum;
    }

}