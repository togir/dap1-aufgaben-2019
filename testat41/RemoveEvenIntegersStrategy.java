public class RemoveEvenIntegersStrategy
implements DoublyLinkedList.DeletionStrategy<Integer>
{
    public boolean select( Integer ref )
    {
        return (int)ref % 2 == 0;
    }
}