public class OneFollowsZeroStrategy implements DoublyLinkedList.InsertionStrategy<Integer> {

    @Override
    public boolean select(Integer ref) {
        return ref == 0;
    }

    @Override
    public Integer insert(Integer ref) {
        return 1;
    }
}