import java.util.Iterator;

public class DoublyLinkedList<T>
implements Iterable<T>
{
    private Element first, last;
    private int size;

    public DoublyLinkedList()
    {
        first = last = null;
        size = 0;
    }

    public int size()
    {
        return size;
    }

    public boolean isEmpty()
    {
        return size == 0;
    }

    public void add( T content ) 
    {
        Element e = new Element( content );
        if ( isEmpty() ) 
        {
            first = last = e;
        }
        else 
        {
            last.connectAsNext( e );
            last = e;
        }
        size++;
    }

    public void addFirst( T content ) 
    {
        Element e = new Element( content );
        if ( isEmpty() ) 
        {
            first = last = e;
        }
        else 
        {
            first.connectAsPrevious( e );
            first = e;
        }
        size++;
    }

    public T getFirst() 
    {
        if ( !isEmpty() )
        {
            return first.getContent();
        }
        else
        {
            throw new IllegalStateException();
        }
    }

    public T get( int index ) 
    {
        if ( index >= 0 && index < size )
        {
            Element current = first;
            for ( int i = 0; i < index; i++ )
            {
                current = current.getNext();
            }
            return current.getContent();
        }
        else
        {
            throw new IllegalStateException();
        }
    }

    public T removeFirst()
    {
        if ( !isEmpty() ) 
        {
            T result = first.getContent();
            if ( first.hasNext() )
            {
                first = first.getNext();
                first.disconnectPrevious();
            }
            else
            {
                first = last = null;
            }
            size--;
            return result;
        }
        else
        {
            throw new IllegalStateException();
        }
    }

    public void showAll()
    {
        Element current = first;
        while ( current != null )
        {
            System.out.print( current.getContent().toString() );
            if ( current != last )
            {
                System.out.print(", ");
            }
            current = current.getNext();
        }
        System.out.println();
    }

        // --- weitere Methoden zum Testen ---
    public void build( T[] elems ) 
    {
        for ( T e : elems ) { add( e ); }      
    }
        
    public String toString()
    {
        String result = "";
        Element current = first;
        while ( current != null )
        {
            result += current.getContent().toString();
            if ( current != last )
            {
                result += ", ";
            }
            current = current.getNext();
        }
        return result;
    }
    
    
    // Iterator

    public Iterator<T> iterator()
    {
        return new ForwardIterator();
    }

    private abstract class ListIterator implements Iterator<T>
    {
        protected Element next;

        protected ListIterator()
        {
            next = first;
        }

        public boolean hasNext()
        {
            return next != null;
        }

        public T next()
        {
            if ( hasNext() )
            {
                T content = next.getContent();
                next = step();
                return content;
            }
            else
            {
                throw new IllegalStateException();
            }
        }

        protected abstract Element step();
    
    }

    private class ForwardIterator extends ListIterator
    {
        protected Element step()
        {
            return next.getNext();
        }

    }

    // strategy

    public static interface SubstitutionStrategy<T>
    {
        T substitute( T ref );
        
        // fuer Testat ergaenzt und eventuell durch Aufgabe vorgegeben
        // bitte ignorieren, wenn in Aufgabenstellung nicht erwaehnt
        default int get(){ return 0; }
    }

    public void substituteAll( SubstitutionStrategy<T> s )
    {
        Element current = first;
        while ( current != null )
        {
            current.setContent( s.substitute( current.getContent() ) );
            current = current.getNext();
        }
    }

    public static interface InspectionStrategy<T>
    {
        void inspect( T ref );
        
        // fuer Testat ergaenzt und eventuell durch Aufgabe vorgegeben
        // bitte ignorieren, wenn in Aufgabenstellung nicht erwaehnt
        default int get(){ return 0; }
    }

    public void inspectAll( InspectionStrategy<T> s )
    {
        Element current = first;
        while ( current != null )
        {
            s.inspect( current.getContent() );
            current = current.getNext();
        }
    }

    public static interface DeletionStrategy<T>
    {
        boolean select( T ref );
    }

    public void deleteSelected( DeletionStrategy<T> s )
    {
        Element current = first;
        while ( current != null )
        {
            Element candidate = current;
            current = current.getNext(); 
            if ( s.select( candidate.getContent() ) )
            {
                remove( candidate );
            }
        }
    }

    public static interface InsertionStrategy<E> {

        boolean select( E ref);
        E insert (E ref);
    }

    public void insertBehindSelected( InsertionStrategy<T> s) {

        Element current = first;
        while ( current != null )
        {
            if(current != this.last && s.select(current.getContent())){
                
                Element next = current.getNext();
                
                current.connectAsNext(new Element (s.insert(current.getContent())));
                current.getNext().connectAsNext(next);
                size++;
                current = current.getNext();

            }

            if(current == this.last && s.select(current.getContent())) {
                this.add(s.insert(current.getContent()));
                return;
            }

            current = current.getNext();
        }
    }

    private void remove( Element e )
    {
        if ( e != null ) 
        {
            if ( e.hasNext() && e.hasPrevious() )
            {
                e.getPrevious().connectAsNext( e.getNext() );
            } else if ( e == first && e.hasNext() )
            {
                first = first.getNext();
                first.disconnectPrevious();
            } else if ( e == last && e.hasPrevious() )
            {
                last = last.getPrevious();
                last.disconnectNext();
            } else {
                first = last = null;
            }
            size--;
        }
    }
    
    // Element

    private class Element
    {
        private T content;
        private Element previous, next;

        public Element( T c )
        {
            content = c;
            previous = next = null;
        }

        public T getContent()
        {
            return content;
        }

        public void setContent( T c )
        {
            content = c;
        }

        public boolean hasNext()
        {
            return next != null;
        }

        public Element getNext()
        {
            return next;
        }

        public void disconnectNext()
        {
            if ( hasNext() ) 
            {
                next.previous = null;
                next = null;
            }
        }

        public void connectAsNext( Element e)
        {
            disconnectNext();
            this.next = e;
            if ( e != null ) 
            {
                e.disconnectPrevious();
                e.previous = this;
            }
        }

        public boolean hasPrevious()
        {
            return previous != null;
        }

        public Element getPrevious()
        {
            return previous;
        }

        public void disconnectPrevious()
        {
            if ( hasPrevious() )
            {
                previous.next = null;
                previous = null;

            }
        }

        public void connectAsPrevious( Element e )
        {
            disconnectPrevious();
            previous = e;
            if ( e != null )
            {
                e.disconnectNext();
                e.next = this;
            }
        }
    }

}   






