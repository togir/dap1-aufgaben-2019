public class InsertFromListStrategy implements DoublyLinkedList.InsertionStrategy<Integer> {

    private DoublyLinkedList<Integer> secondList;

    public InsertFromListStrategy(DoublyLinkedList<Integer> list) {
        this.secondList = list;
    }

    @Override
    public boolean select(Integer ref) {
        return true;
    }

    @Override
    public Integer insert(Integer ref) {
        Integer tmp = this.secondList.getFirst();
        this.secondList.removeFirst();

        return tmp;
    }
}