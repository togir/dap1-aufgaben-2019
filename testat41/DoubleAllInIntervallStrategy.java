public class DoubleAllInIntervallStrategy implements DoublyLinkedList.SubstitutionStrategy<Integer> {

    private int from, to, index;

    public DoubleAllInIntervallStrategy(int from, int to) {
        this.from = from;
        this.to = to;
        index = 0;
    }

    public Integer substitute(Integer ref) {
        if(from <= index && to >= index) {
            index++;
            return ref * 2;
        }

        index++;
        return ref;
    }
}