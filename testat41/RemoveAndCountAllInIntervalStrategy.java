public class RemoveAndCountAllInIntervalStrategy implements DoublyLinkedList.DeletionStrategy<Integer> {
    private int from, to, index, quantity;

    public RemoveAndCountAllInIntervalStrategy(int from, int to) {
        this.from = from;
        this.to = to;
        index = 0;
        quantity = 0;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public boolean select(Integer el){

        if(
            this.index >= this.from && 
            this.index <= this.to
        ) {
            quantity++;
            index++;
            return true;
        } 
        index++;
        return false;
    }
}