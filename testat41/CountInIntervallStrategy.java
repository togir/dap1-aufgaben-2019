public class CountInIntervallStrategy implements DoublyLinkedList.InspectionStrategy<Integer> {

    private int from, to, index, quantity, x;

    public CountInIntervallStrategy(int from, int to, int x) {
        this.from = from;
        this.to = to;
        this.index = 0;
        this.x = x;
        this.quantity = 0;
    }

    public void inspect(Integer ref) {

        if(to >= index && from <= index && x == ref) {
            quantity = quantity +1;
        }

        index = index +1;
    }

    public int getQuantity() {
        return this.quantity;
    }
}