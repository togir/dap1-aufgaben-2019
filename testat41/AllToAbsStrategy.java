public class AllToAbsStrategy implements DoublyLinkedList.SubstitutionStrategy<Integer> {

    public Integer substitute(Integer ref) {
        return  Math.abs(ref);
    }
}