public class CountStudentGt100Strategy 
implements DoublyLinkedList.InspectionStrategy<Student>
{
    private int quantity;
    
    public CountStudentGt100Strategy() { quantity = 0; }
    
    public void inspect( Student ref )
    {
        if ( (ref).getRegistrationNo() >100 )
        {
            quantity++;
        }
    }
    
    public int getQuantity() { return quantity; }
}