public class Testumgebung {

    public static void main(String args[])
    {
        test();
    }

    public static void test()
    {

        DoublyLinkedList<Integer> ints = new DoublyLinkedList<>();
        Integer[] a = { 5, 0, -9, 11, 2, 0, 7, -12, 5, 22, 1, -4, -11, 2, 3, 7, -4, 0 };
        ints.build( a );

        /*
        DoublyLinkedList<Integer> ints2 = new DoublyLinkedList<>();
        Integer[] b = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        ints2.build( b );
        */

        System.out.print( "Ausgangsliste: " ); ints.showAll();
        System.out.println( "Laenge: " + ints.size() );
        System.out.println();
        

        //Anlegen des Strategieobjekts
        CountEveryThirdInIntervallStrategy s = new CountEveryThirdInIntervallStrategy(2, 10);

        //Auswahl dieser Testzeile muss vom Studierenden in Abhaengigkeit
        //von der Aufgabenstellung vorgenommen werden
        ints.inspectAll( s );
        //ints.substituteAll( s );
        //ints.insertBehindSelected( s );
        //ints.deleteSelected(s);

        //Ausfuehrung dieser Testzeile muss vom Studierenden in Abhaengigkeit
        //von der Aufgabenstellung ermoeglicht werden
        System.out.print( "Ergebnisliste: " ); ints.showAll();
        System.out.println( "Ergebnis von get(): " + s.getSum() );
        System.out.println( "Laenge: " + ints.size() );
        System.out.println();

    }
}
