public class ReplaceEveryThirdWithDoubleStrategy implements DoublyLinkedList.SubstitutionStrategy<Integer> {

    private int count;

    public ReplaceEveryThirdWithDoubleStrategy() {
        this.count = 0;
    }

    public Integer substitute(Integer el) {

        count = count + 1;

        if(this.count != 0 && this.count % 3 == 0) {
            count = 0;
            return el *2;
        }

        return el;
    }
}