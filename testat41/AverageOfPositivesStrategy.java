public class AverageOfPositivesStrategy implements DoublyLinkedList.InspectionStrategy<Integer> {

    private int sumOfPositives, quantityOfPostives;

    public void inspect(Integer ref) {
        if((Integer) ref > 0) {
            sumOfPositives = sumOfPositives + (Integer) ref;
            quantityOfPostives++;
        }
    }

    public double getAverage() {
        return sumOfPositives / quantityOfPostives;
    }

}