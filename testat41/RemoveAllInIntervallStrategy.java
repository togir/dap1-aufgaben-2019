public class RemoveAllInIntervallStrategy implements DoublyLinkedList.DeletionStrategy<Integer> {

    private int from, to, index;

    public RemoveAllInIntervallStrategy(int from, int to) {
        this.from = from;
        this.to = to;
        this.index = 0;
    }

    public boolean select(Integer el) {
        
        if(
            this.index >= this.from && 
            this.index <= this.to
        ) {
            index++;
            return true;
        } 
        index++;
        return false;
    }

    
}