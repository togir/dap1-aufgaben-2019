public class DoubleIntegersStrategy 
implements DoublyLinkedList.SubstitutionStrategy<Integer>
{
    public Integer substitute( Integer ref )
    {
        return 2 * ref;
    }
}