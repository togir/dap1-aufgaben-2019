public class SubtotalOfThreeElementsStrategy implements DoublyLinkedList.InsertionStrategy<Integer> {
    
    private int subSum, index;

    public SubtotalOfThreeElementsStrategy() {
        this.subSum = 0;
        this.index = 0;
    }

    @Override
    public boolean select(Integer ref) {
        this.subSum = this.subSum + ref;
        this.index = this.index +1;

        return (index % 3 == 0 && index != 0);
    }

    @Override
    public Integer insert(Integer ref) {
        int tmp = this.subSum;
        this.subSum = 0;
        return tmp;
    }
}