public class RemoveSmallerThanPredecessorStrategy implements DoublyLinkedList.DeletionStrategy<Integer> {
    
    private Integer pred;

    public RemoveSmallerThanPredecessorStrategy() {
        this.pred = null;
    }

    public boolean select(Integer el) {

        if(this.pred != null && this.pred > el) {
            return true;
        }

        this.pred = el;
        return false;
    }

}