public class IntegerSummationStrategy 
implements DoublyLinkedList.InspectionStrategy<Integer>
{
    private int sum;
    
    public IntegerSummationStrategy() { sum = 0; }
        
    public void inspect( Integer ref )
    {
        sum += ref;
    }
    
    public int getSum()
    {
        return sum;
    }
}