public class RemoveEveryThirdInIntervallStrategy implements DoublyLinkedList.DeletionStrategy<Integer> {

    private int count, from, to, index;

    public RemoveEveryThirdInIntervallStrategy(int from, int to) {
        this.count = index = 0;
        this.from = from;
        this.to = to;
    }

    public boolean select(Integer el) {

        if(this.index >= this.from && this.index <= this.to) {
            count = count +1;

            if(count % 3 == 0) {
                count = 0;
                return true;
            }
        }
        index = index +1;
        return false;
    }
}