public class RemoveAllNegativesStrategy implements DoublyLinkedList.DeletionStrategy<Integer> {

    public boolean select(Integer ref) {
        
        return ref < 0;
    }
}