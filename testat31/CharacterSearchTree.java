public class CharacterSearchTree
{ 
    private HuffmanTriple content;
    private CharacterSearchTree leftChild, rightChild;

    public CharacterSearchTree() 
    {
        content = null;
        leftChild = null;
        rightChild = null;
    }

    // Aufgabe 1
    public CharacterSearchTree (char[] c) {

        for (int i = 0; i < c.length; i++) {
            this.add(c[i]);
        }
    }

    public HuffmanTriple getContent()
    {
        if ( !isEmpty() )
        {
            return content;
        } else {
            throw new IllegalStateException();
        }
    }

    public boolean isEmpty() 
    {
        return content == null;
    }

    public boolean isLeaf() 
    {
        return !isEmpty() && leftChild.isEmpty() && rightChild.isEmpty();
    }

    // Aufgabe 2
    public void add(char t, int q, String c) {

        if(isEmpty()) {
            content = new HuffmanTriple(t, q);
            content.setCode(c);
            leftChild = new CharacterSearchTree();
            rightChild = new CharacterSearchTree();
            return;
        }

        if( content.getToken() > t) {
            leftChild.add(t, q, c);
        }
        else if(content.getToken() < t) {
            rightChild.add(t, q, c);
        } 
        else {
            for(int i=0; i < q; i++) {
                content.incrementQuantity();
            }
            content.setCode(c);
        }




    }

    public void add( char t )
    {
        if ( isEmpty() ) 
        {
            content = new HuffmanTriple( t );
            leftChild = new CharacterSearchTree();
            rightChild = new CharacterSearchTree();
        }
        else
        {
            if ( content.getToken() > t )
            {
                leftChild.add( t );
            }
            else if ( content.getToken() < t )
            {
                rightChild.add( t );
            }
            else
            {
                content.incrementQuantity();
            }
        }
    }

    public void iterativeAdd( char t )
    {
        CharacterSearchTree current = this;
        while ( !current.isEmpty() && current.content.getToken() != t )
        {
            if ( current.content.getToken() > t )
            {
                current = current.leftChild;
            }
            else
            {
                current = current.rightChild;
            }
        }
        if ( current.isEmpty() ) 
        {
            current.content = new HuffmanTriple( t );
            current.leftChild = new CharacterSearchTree();
            current.rightChild = new CharacterSearchTree();
        }
        else
        {
            current.content.incrementQuantity();
        }
    }

    public String getCode( char t )
    {
        if ( !isEmpty() ) 
        {
            if ( content.getToken() > t )
            {
                return leftChild.getCode( t );
            }
            else if ( content.getToken() < t )
            {
                return rightChild.getCode( t );
            }
            else
            {
                return content.getCode();
            }
        }
        else
        {
            throw new IllegalStateException();
        }
    }
    
    public int size() 
    {
        if ( isEmpty() ) 
        {
            return 0;
        }
        else
        {
            return 1 + leftChild.size() + rightChild.size();
        }       
    }

    public void show()
    {
        if ( !isEmpty() ) 
        {
            leftChild.show();
            System.out.println( content.toString() );
            rightChild.show();
        }
    }

    public HuffmanTriple[] toArray()
    {
        if ( !isEmpty() ) 
        {
            HuffmanTriple[] collector = new HuffmanTriple[size()];
            toArray( collector, 0 );
            return collector;
        }
        return new HuffmanTriple[0];
    }

    private int toArray( HuffmanTriple[] collector, int index ) 
    {
        if ( !isEmpty() )
        {
            index = leftChild.toArray( collector, index );
            collector[index] = content;
            index = rightChild.toArray( collector, index + 1 );
        }
        return index;
    }  

    // Aufgabe 3
    public void showPreOrder() {
        if( isLeaf()) {
            System.out.println('*' + this.content.toString());
        } else {
            System.out.println(this.content.toString());
        }

        if(this.leftChild.content != null) {
            this.leftChild.showPreOrder();
        }

        if(this.rightChild.content != null) {
            this.rightChild.showPreOrder();
        }

    }

    // Aufgabe 4
    public int height() {
        if(isEmpty()) {
            return 0;
        }

        if(isLeaf()) {
            return 1;
        }

        if(leftChild.height() > rightChild.height()) {
            return leftChild.height() +1;
        } else {
            return rightChild.height() +1;
        }


    }

    // Aufgabe 5
    public int countCharacters() {
    
        if(isEmpty()) {
            return 0;
        }

        if(isLeaf()) {
            return content.getQuantity();
        }

        return content.getQuantity() + leftChild.countCharacters() + rightChild.countCharacters();

    
    }

    // Aufgabe 6
    public int longestCode() {

        if(isEmpty()) {
            return 0;
        }

        if(isLeaf()) {
            return this.content.getCode().length();
        }

        if(leftChild.longestCode() > this.content.getCode().length()) {
            
            if(leftChild.longestCode() > rightChild.longestCode()) {
                return leftChild.longestCode();
            }

        }

        if(rightChild.longestCode() > this.content.getCode().length()) {
            return rightChild.longestCode();
        }
        
        return this.content.getCode().length();  
    }

    // Aufgabe 7
    public HuffmanTriple minimum() {
        
        CharacterSearchTree newTree = this;

        if(leftChild.isEmpty()) {
            return this.getContent();
        }

        while(!newTree.leftChild.isEmpty()) {
            newTree = newTree.leftChild;
        }

        return newTree.getContent();
    }

    // Aufgabe 8
    public boolean hasOnlyCompleteNode() {
        if(isEmpty()) {
            return true;
        }

        if(isLeaf()) {
            return true;
        }

        if(!rightChild.isEmpty() && leftChild.isEmpty()) {
            return false;
        }

        if(!leftChild.isEmpty() && rightChild.isEmpty()) {
            return false;
        }

        if(leftChild.hasOnlyCompleteNode() && rightChild.hasOnlyCompleteNode()) {
            return true;
        } 
        return false;

    }

    // Aufgabe 9
    public boolean containsCharacter(char t) {
        if(isEmpty()) {
            return false;
        }

        if(this.getContent().getToken() == t) {
            return true;
        }

        return rightChild.containsCharacter(t) || leftChild.containsCharacter(t);
    }

    // Aufgabe 10
    public boolean equalStructure(CharacterSearchTree cst) {

        if(this.isEmpty() != cst.isEmpty()) {
            return false;
        }

        if(this.isEmpty() && cst.isEmpty()) {
            return true;
        }


        return this.leftChild.equalStructure(cst.leftChild) && this.rightChild.equalStructure(cst.rightChild);    

    }

    // Aufagabe 11
    public CharacterSearchTree rotateNodeToRight() {
        if(this.isEmpty() || this.leftChild.isEmpty()) {
            return this;
        }

        CharacterSearchTree newTree = this.leftChild;
        this.leftChild = newTree.rightChild;
        newTree.rightChild = this;

        return newTree;
    }

    // Aufgabe 12
    public boolean samePath(char t1, char t2) {
        if(this.isEmpty()) {
            return false;
        }

        if(this.getContent().getToken() == t1) {
            return this.rightChild.containsCharacter(t2) || this.leftChild.containsCharacter(t2);
        }

        if(this.getContent().getToken() == t2) {
            return this.rightChild.containsCharacter(t1) || this.leftChild.containsCharacter(t1);
        }

        return this.leftChild.samePath(t1, t2) || this.rightChild.samePath(t1, t2);
        
    }

    // Aufgabe jorgo1 Wie viele Buchstaben gibt es
    public int wvb() {
        if(isLeaf()) {
            return 1;
        }

        if(isEmpty()) {
            return 0;
        }

        return this.leftChild.wvb() + this.rightChild.wvb() + 1;
    }

    // Aufgabe jorgo2 Anzahl aller geraden quantities
    public int gq() {
        if(isEmpty()) {
            return 0;
        } 

        if(this.content.getQuantity()  < 10) {
            return this.leftChild.gq() + this.rightChild.gq() + 1;
        }

        return this.leftChild.gq() + this.rightChild.gq();

    }

    // Aufgabe jorgo 3
    public CharacterSearchTree raiseQuantities(int x) {

        if(isEmpty()) {
            return this;
        }

        if(isLeaf()) {
            for(int i=0; i<x; i++) {
                content.incrementQuantity();
            }
            return this;
        }

        for(int i=0; i<x; i++) {
            content.incrementQuantity();
        }
        this.leftChild.raiseQuantities(x);
        this.rightChild.raiseQuantities(x);
        return this;
    }






    /** Aufgabe jorgo4 Kinder löschen
    public CharacterSearchTree deleteChild(char t) {

        if(this.leftChild.getContent().getToken() == t) {

        }

        if(this.rightChild.getContent().getToken() == t) {
            CharacterSearchTree leftTree = this.rightChild.leftChild;
            CharacterSearchTree rightTree = this.rightChild.leftChild;

            
        }
    }

    */
    // todo convert trees to array
    // mörge the two arrays
    // add every elemt of the array list to new node.
    // replace node that should be deletet
    // PROFIT!!!!


    public void showInorder() {
        
        if(isEmpty()) {
            return;
        }

        if(isLeaf()) {
            System.out.println(this.content.toString());
            return;
        }

        this.leftChild.showInorder();
        System.out.println(this.content.toString());
        this.rightChild.showInorder();
    }
    
    public void showPostorder() {
        if(isEmpty()) {
            return;
        }
        
        if(isLeaf()) {
            System.out.println(this.content.toString());
            return;
        }
        
        this.leftChild.showPostorder();
        this.rightChild.showPostorder();
        System.out.println(this.content.toString());

    }

    public int qtLargerThanTen() {

        if(isEmpty()) {
            return 0;
        }

        if(isLeaf() && this.content.getQuantity() >= 10) {
            return 1;
        }

        if(this.content.getQuantity() >= 10) {
            return this.leftChild.qtLargerThanTen() + this.rightChild.qtLargerThanTen() + 1;
        }
        return this.leftChild.qtLargerThanTen() + this.rightChild.qtLargerThanTen();

    }

}
