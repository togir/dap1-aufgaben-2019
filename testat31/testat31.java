import static java.lang.System.out;
class Testat31 {
    public static void main(String[] args) {
        
        CharacterSearchTree tree = geneTreeJ();


        //tree.showPreOrder();
        //out.println( tree.height() );
        //out.println(tree.countCharacters());
        out.println(tree.qtLargerThanTen());


    }

    private static CharacterSearchTree generateTree() {
        CharacterSearchTree tree = new CharacterSearchTree();

        tree.add('j', 1, "0000000000000111");
        tree.add('c', 3, "1100000000");
        tree.add('d', 2, "1");
        tree.add('b', 3, "10");
        tree.add('z', 56, "01010");
        tree.add('t', 3, "01");
        tree.add('k');
        tree.add('a');
        tree.add('m', 2, "0101000000");

        return tree;

    }

    private static CharacterSearchTree geneTreeJ() {
        CharacterSearchTree tree = new CharacterSearchTree();

        tree.add('a',1,"192839");
        
        tree.add('c',11,"11");
        
        tree.add('d',10,"1");
        
        tree.add('b',3,"10111111");

        tree.add('z',2,"10101419827");

        return tree;
    }

    private static CharacterSearchTree generateEqualTree() {
        
        CharacterSearchTree tree = new CharacterSearchTree();

        tree.add('j', 1, "0000000000000111");
        tree.add('c', 2, "1100000000");
        tree.add('d', 2, "1");
        tree.add('b', 3, "10");

        tree.add('y', 3, "1100000000");
        tree.add('x', 2, "1");
        tree.add('z', 3, "10");
        
        return tree;
    }
    private static CharacterSearchTree uTree() {

        CharacterSearchTree tree = new CharacterSearchTree();

        tree.add('k');
        tree.add('d');
        tree.add('b');
        tree.add('f');
        tree.add('h');
        tree.add('s');
        tree.add('o');
        tree.add('x');
        tree.add('p');
        tree.add('v');

        return tree;


    }

}